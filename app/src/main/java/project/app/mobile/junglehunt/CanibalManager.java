package project.app.mobile.test2;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;

import java.util.ArrayList;

public class CanibalManager {

    public ArrayList<Canibal> canibals;
    public Point canibalPoint = new Point( 0, 0 );

    private int xPos = Constants.SCREEN_WIDTH / 3;
    private int yPos = (int) ( Constants.SCREEN_HEIGHT * 0.58 );
    private int incrementY = 10;
    private int incrementX = 5;
    private int random = 0;

    private long startTime, elapsedTime;

    private boolean positionChanged, jumped = false;

    public CanibalManager(){
        canibals = new ArrayList<>();
        canibalPoint.set( xPos, yPos );
        canibals.add( new Canibal( 0, 0, (int) (Constants.SCREEN_WIDTH * 0.04), (int) (Constants.SCREEN_HEIGHT * 0.14), canibalPoint ) );
        startTime = System.currentTimeMillis();
    }

    public boolean collide(final Player player ){
        for( Canibal canibal : canibals){
            if( canibal.getCanibal().intersects( player.getPlayer(), canibal.getCanibal() ) ){
                return true;
            }
        }
        return false;
    }

    public void resetPosition(){
        canibalPoint.set( xPos, yPos );
        canibals.get( canibals.size() -1 ).resetPosition( canibalPoint );
    }

    private void changePosition(){
        incrementX = -incrementX;
        positionChanged = false;
    }

    private void jump(){
        if( jumped == true ) {
            yPos -= incrementY;
            if( yPos <= Constants.SCREEN_HEIGHT * 0.3 ) {
                yPos = (int) ( Constants.SCREEN_HEIGHT * 0.3 );
                jumped = false;
            }
        } else if( jumped == false ){
            yPos += incrementY;
            if( yPos >= Constants.SCREEN_HEIGHT * 0.58 ){
                yPos = (int) ( Constants.SCREEN_HEIGHT * 0.58 );
            }
        }
    }

    public void update() {
        elapsedTime = System.currentTimeMillis();
        if( elapsedTime - startTime >= 2000 ) {
            positionChanged = true;
            jumped = true;
            random = (int) ((Math.random() * 3) + 1);
            startTime = System.currentTimeMillis();
        }
        xPos += incrementX;
        canibalPoint.set( xPos, yPos );

        for( Canibal canibal : canibals){
            canibal.update( canibalPoint );
        }

        if( random >= 2 ){
            jump();
        }

        if( random == 1 && positionChanged ) {
            changePosition();
        }

        if( canibals.get( canibals.size() -1 ).getCanibal().right >= 4 * Constants.SCREEN_WIDTH / 5 || canibals.get( canibals.size() -1 ).getCanibal().left <= Constants.SCREEN_WIDTH / 5 ) {
            incrementX = -incrementX;
        }

        if( canibals.get( canibals.size() - 1 ).getCanibal().left >= Constants.SCREEN_WIDTH ){
            canibals.add( new Canibal( 0, 0, (int) (Constants.SCREEN_WIDTH * 0.04), (int) (Constants.SCREEN_HEIGHT * 0.14), canibalPoint ) );
        }

        if( canibals.get( canibals.size() -1 ).getCanibal().left >= Constants.SCREEN_WIDTH ){
            canibals.remove( canibals.size() -1 );
        }
    }

    public void draw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor( Color.GREEN );
        for( Canibal canibal : canibals){
            canibal.draw( canvas );
        }
    }
}
