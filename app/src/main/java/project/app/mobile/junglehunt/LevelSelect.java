package project.app.mobile.test2;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

public class LevelSelect extends View {

    public LevelSelect(Context context) {
        super(context);
    }

    @Override
    protected void onDraw( Canvas canvas ) {
        super.onDraw(canvas);
        Paint paint = new Paint();
        paint.setTextSize( Constants.SCREEN_WIDTH / 10 );

        paint.setColor( Color.RED );
        canvas.drawRect( 0, 0, Constants.SCREEN_WIDTH / 2, Constants.SCREEN_HEIGHT / 2, paint );
        paint.setColor( Color.BLACK );
        canvas.drawText( "1 level", Constants.SCREEN_WIDTH / 8, Constants.SCREEN_HEIGHT / 4, paint );

        paint.setColor( Color.BLUE );
        canvas.drawRect( Constants.SCREEN_WIDTH / 2, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT / 2, paint );
        paint.setColor( Color.BLACK );
        canvas.drawText( "2 level", (float) (Constants.SCREEN_WIDTH / 1.75), Constants.SCREEN_HEIGHT / 4, paint );

        paint.setColor( Color.YELLOW );
        canvas.drawRect( 0, Constants.SCREEN_HEIGHT / 2, Constants.SCREEN_WIDTH / 2, Constants.SCREEN_HEIGHT, paint );
        paint.setColor( Color.BLACK );
        canvas.drawText( "3 level", Constants.SCREEN_WIDTH / 8, (float) (Constants.SCREEN_HEIGHT / 1.25), paint );

        paint.setColor( Color.GREEN );
        canvas.drawRect( Constants.SCREEN_WIDTH / 2, Constants.SCREEN_HEIGHT / 2, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT, paint );
        paint.setColor( Color.BLACK );
        canvas.drawText( "4 level", (float) (Constants.SCREEN_WIDTH / 1.75), (float) (Constants.SCREEN_HEIGHT / 1.25), paint );
    }

}
