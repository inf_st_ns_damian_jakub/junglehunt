package project.app.mobile.test2;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;

import java.util.ArrayList;

/**
 * Created by Dasmitian on 27.03.2018.
 */

public class Player implements GameObject {

    public ArrayList<Rect> life;
    private Rect player;
    private Rect r = new Rect();
    public int color = Color.RED;
    public int score = 0;

    public Rect getPlayer(){
        return player;
    }

    public Player( Rect player ){
        this.player = player;
        addLife();
    }

    private void addLife(){
        life = new ArrayList<>();
        for( int i = 0; i < 5; i++ ){
            life.add( new Rect(
                    (int) (Constants.SCREEN_WIDTH * 0.04) + ( i * (int) (Constants.SCREEN_WIDTH * 0.08) ),
                    (int) (Constants.SCREEN_HEIGHT * 0.07),
                    (int) (Constants.SCREEN_WIDTH * 0.08) + ( i * (int) (Constants.SCREEN_WIDTH * 0.08) ),
                    (int) (Constants.SCREEN_HEIGHT * 0.14) ));
        }
    }

    public void removeLife(){
        life.remove( life.size() - 1 );
    }

    public void gameOverText(Canvas canvas, Paint paint, String text ){
        paint.setTextAlign( Paint.Align.LEFT );
        canvas.getClipBounds( r );
        int cHeight = r.height();
        int cWidth = r.width();
        paint.getTextBounds( text, 0, text.length(), r);
        float x = cWidth / 2f - r.width() / 2f - r.left;
        float y = cHeight / 2f + r.height() / 2f - r.bottom;
        canvas.drawText( text, x, y, paint );
    }

    @Override
    public void draw( Canvas canvas ){
        Paint paint = new Paint();
        paint.setColor( color );
        canvas.drawRect( player, paint );
        for( Rect rect : life ){
            canvas.drawRect( rect, paint );
        }

        Paint textPaint = new Paint();
        textPaint.setTextSize( (float) (Constants.SCREEN_HEIGHT * 0.1) );
        textPaint.setColor( Color.BLACK );
        canvas.drawText("" + score, Constants.SCREEN_WIDTH / 2, (float) ( Constants.SCREEN_HEIGHT * 0.1 ), textPaint );
    }

    @Override
    public void update() {

    }

    public void update( Point point ){
        player.set( point.x - player.width()/2, point.y - player.height()/2, point.x + player.width()/2, point.y + player.height()/2 );
    }
}
