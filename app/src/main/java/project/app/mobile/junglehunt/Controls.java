package project.app.mobile.test2;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

public class Controls {
    private double radius = Constants.SCREEN_WIDTH * 0.1;
    private int circleY = Constants.SCREEN_HEIGHT - (int) radius;
    private int rectWidth = (int) (radius * 0.3);
    public boolean drawActionButton = false;
    public boolean drawMoveButton = false;
    public boolean drawUpDown = false;

    public Rect goUp;
    public Rect goDown;
    public Rect goLeft;
    public Rect goRight;
    public Rect action;



    public Controls(){
        goUp = new Rect((int) ( radius - rectWidth ), circleY - 3*rectWidth, (int) (radius + rectWidth), circleY - rectWidth );
        goDown = new Rect((int) ( radius - rectWidth ), circleY + rectWidth, (int) (radius + rectWidth), circleY + 3*rectWidth );
        goLeft = new Rect((int) ( radius - 3*rectWidth ), circleY - rectWidth, (int) (radius - rectWidth), circleY + rectWidth );
        goRight = new Rect((int) ( radius + rectWidth ), circleY - rectWidth, (int) (radius + 3*rectWidth), circleY + rectWidth );
        action = new Rect( (int) ( Constants.SCREEN_WIDTH - 2 * radius ), circleY - (int) radius, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT );
    }

    public void draw( Canvas canvas){
        Paint paint = new Paint();

        if( drawActionButton == true ) {
            paint.setColor(Color.GRAY);
            paint.setAlpha(200);
            canvas.drawCircle(Constants.SCREEN_WIDTH - (int) radius, Constants.SCREEN_HEIGHT - (int) radius, (int) radius, paint);
            paint.setAlpha(0);
            canvas.drawRect(action, paint);
        }

        if( drawMoveButton == true ){
            paint.setColor(Color.GRAY);
            paint.setAlpha(200);
            canvas.drawCircle((int) radius, circleY, (int) radius, paint);
            paint.setColor(Color.BLUE);
            if( drawUpDown == true ) {
                canvas.drawRect(goUp, paint);
                canvas.drawRect(goDown, paint);
            }
            canvas.drawRect(goLeft, paint);
            canvas.drawRect(goRight, paint);
        }
    }
}
