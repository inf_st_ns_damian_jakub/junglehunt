package project.app.mobile.test2;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;

import java.util.ArrayList;

public class Crocodile implements GameObject {

    public Rect crocodile;


    public Crocodile( int left, int top, int right, int bottom, Point point ){
        crocodile = new Rect( left, top, right, bottom );
        crocodile.set( point.x - crocodile.width()/2, point.y - crocodile.height()/2, point.x + crocodile.width()/2, point.y + crocodile.height()/2 );
    }

    public Rect getCrocodile(){
        return crocodile;
    }

    public void incrementX( float x ){
        crocodile.left += x;
        crocodile.right += x;
    }

    @Override
    public void draw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor( Color.GREEN );
        canvas.drawRect( crocodile, paint );
    }

    @Override
    public void update() {
    }

}
