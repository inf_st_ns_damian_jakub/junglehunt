package project.app.mobile.test2;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;

public class Canibal implements GameObject {

    private Rect canibal;

    public Canibal( int left, int top, int right, int bottom, Point point ){
        canibal = new Rect( left, top, right, bottom );
        canibal.set( point.x - canibal.width()/2, point.y - canibal.height()/2, point.x + canibal.width()/2, point.y + canibal.height()/2 );
    }

    public Rect getCanibal() {
        return canibal;
    }

    public int getRight(){
        return canibal.right;
    }

    public int getTop(){
        return canibal.top;
    }

    public int getBottom() { return  canibal.bottom; }

    public void resetPosition( Point point ){
        canibal.set( point.x - canibal.width()/2, point.y - canibal.height()/2 - 10, point.x + canibal.width()/2, point.y + canibal.height()/2 -10 );
    }

    @Override
    public void draw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor( Color.BLACK );
        canvas.drawRect( canibal, paint );
    }

    @Override
    public void update() {

    }

    public void update( Point point ){
        canibal.set( point.x - canibal.width()/2, point.y - canibal.height()/2, point.x + canibal.width()/2, point.y + canibal.height()/2 );
    }
}
