package project.app.mobile.test2;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;

public class LevelSelectActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE );
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics( dm );
        Constants.SCREEN_WIDTH = dm.widthPixels;
        Constants.SCREEN_HEIGHT = dm.heightPixels;

        setContentView( new LevelSelect( this ) );
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event ){

        int eventAction = event.getAction();
        switch ( eventAction ){
            case MotionEvent.ACTION_DOWN:
                if( event.getX() < Constants.SCREEN_WIDTH / 2 && event.getY() < Constants.SCREEN_HEIGHT / 2 ) {
                    Intent intent = new Intent(this, FirstLevelActivity.class);
                    startActivity(intent);
                } else if( event.getX() > Constants.SCREEN_WIDTH / 2 && event.getY() < Constants.SCREEN_HEIGHT / 2 ){
                    Intent intent = new Intent(this, SecondLevelActivity.class);
                    startActivity(intent);
                } else if( event.getX() < Constants.SCREEN_WIDTH / 2 && event.getY() > Constants.SCREEN_HEIGHT / 2 ){
                    Intent intent = new Intent(this, ThirdLevelActivity.class);
                    startActivity(intent);
                } else if( event.getX() > Constants.SCREEN_WIDTH / 2 && event.getY() > Constants.SCREEN_HEIGHT / 2 ){
                    Intent intent = new Intent(this, FourthLevelActivity.class);
                    startActivity(intent);
                }
                break;
            case MotionEvent.ACTION_UP:
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed(){ }
}
