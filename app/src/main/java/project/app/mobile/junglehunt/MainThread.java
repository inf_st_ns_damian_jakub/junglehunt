package project.app.mobile.test2;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

/**
 * Created by Dasmitian on 27.03.2018.
 */

public class MainThread extends Thread {
    public static final int MAX_FPS = 60;
    private double averageFPS;
    private SurfaceHolder surfaceHolder;
    private FirstLevel gamePanel;
    private SecondLevel gamePanel2;
    private ThirdLevel gamePanel3;
    private FourthLevel gamePanel4;
    private boolean running;
    public static Canvas canvas;
    private boolean firstLevelRun, secondLevelRun, thirdLevelRun, fourthLevelRun = false;

    public void setRunning( boolean running ){
        this.running = running;
    }

    public MainThread(SurfaceHolder surfaceHolder, FirstLevel gamePanel ){
        super();
        this.surfaceHolder = surfaceHolder;
        this.gamePanel = gamePanel;
        firstLevelRun = true;
        secondLevelRun = false;
        thirdLevelRun = false;
        fourthLevelRun = false;
    }

    public MainThread(SurfaceHolder surfaceHolder, SecondLevel gamePanel2 ){
        super();
        this.surfaceHolder = surfaceHolder;
        this.gamePanel2 = gamePanel2;
        firstLevelRun = false;
        secondLevelRun = true;
        thirdLevelRun = false;
        fourthLevelRun = false;
    }

    public MainThread(SurfaceHolder surfaceHolder, ThirdLevel gamePanel3 ){
        super();
        this.surfaceHolder = surfaceHolder;
        this.gamePanel3 = gamePanel3;
        firstLevelRun = false;
        secondLevelRun = false;
        thirdLevelRun = true;
        fourthLevelRun = false;
    }

    public MainThread(SurfaceHolder surfaceHolder, FourthLevel gamePanel4 ){
        super();
        this.surfaceHolder = surfaceHolder;
        this.gamePanel4 = gamePanel4;
        firstLevelRun = false;
        secondLevelRun = false;
        thirdLevelRun = false;
        fourthLevelRun = true;
    }

    @Override
    public void run(){
        long startTime;
        long timeMillis = 1000/MAX_FPS;
        long waitTime;
        long frameCount = 0;
        long totalTime = 0;
        long targetTime = 1000/MAX_FPS;

        while( running ){
            startTime = System.nanoTime();
            canvas = null;

            try{
                canvas = this.surfaceHolder.lockCanvas();
                synchronized( surfaceHolder ){
                    if( firstLevelRun == true ) {
                        this.gamePanel.update();
                        this.gamePanel.draw(canvas);
                    }else if( secondLevelRun == true ) {
                        this.gamePanel2.update();
                        this.gamePanel2.draw(canvas);
                    } else if( thirdLevelRun == true ) {
                        this.gamePanel3.update();
                        this.gamePanel3.draw(canvas);
                    } else if( fourthLevelRun == true ){
                        this.gamePanel4.update();
                        this.gamePanel4.draw(canvas);
                    }
                }
            }catch ( Exception e ){
                e.printStackTrace();
            }finally {
                if( canvas != null ){
                    try{
                        surfaceHolder.unlockCanvasAndPost( canvas );
                    }catch( Exception e ) { e.printStackTrace(); }
                }
            }
            timeMillis = ( System.nanoTime() - startTime )/1000000;
            waitTime = targetTime - timeMillis;
            try{
                if( waitTime > 0 ){
                    this.sleep( waitTime );
                }
            }catch( Exception e ){ e.printStackTrace(); }

            totalTime += System.nanoTime() - startTime;
            frameCount++;
            if( frameCount == MAX_FPS ){
                averageFPS = 1000/( ( totalTime / frameCount )/1000000 );
                frameCount = 0;
                totalTime = 0;
                System.out.println( averageFPS );
            }
        }
    }
}
