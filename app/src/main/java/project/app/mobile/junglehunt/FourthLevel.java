package project.app.mobile.test2;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class FourthLevel extends SurfaceView implements SurfaceHolder.Callback{

    private MainThread thread;
    private Player player;
    private Controls controls;
    private CanibalManager canibalManager;

    private Point playerPoint;
    private int playerXPos = 4 * Constants.SCREEN_WIDTH / 5;
    private int playerYPos = (int) ( Constants.SCREEN_HEIGHT * 0.615 );

    private boolean jump = false;
    private boolean moveLeft, moveRight = false;

    private long startTime, elapsedTime;

    public FourthLevel( Context context ){
        super( context );

        getHolder().addCallback( this );


        thread = new MainThread( getHolder(), this );

        player = new Player( new Rect( 0, 0, (int) (Constants.SCREEN_WIDTH * 0.04), (int) (Constants.SCREEN_HEIGHT * 0.07) ) );
        playerPoint = new Point(playerXPos, playerYPos);

        controls = new Controls();
        controls.drawActionButton = true;
        controls.drawMoveButton = true;

        canibalManager = new CanibalManager();
    }

    @Override
    public void surfaceChanged( SurfaceHolder holder, int format, int width, int height ){

    }

    @Override
    public void surfaceCreated( SurfaceHolder holder ){
        thread = new MainThread( getHolder(), this );
        thread.setRunning( true );
        thread.start();
    }


    @Override
    public void surfaceDestroyed( SurfaceHolder holder ){
        boolean retry = true;
        while( retry ){
            try{
                thread.setRunning( false );
                thread.join();
            } catch( Exception e ){ e.printStackTrace(); }
            retry = false;
        }
    }

    @Override
    public boolean onTouchEvent( MotionEvent event ) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (controls.action.contains((int) event.getX(), (int) event.getY()) && playerYPos == (int) (Constants.SCREEN_HEIGHT * 0.615) ) {
                    jump = true;
                    startTime = System.currentTimeMillis();
                }
                if( controls.goLeft.contains( (int)event.getX(), (int)event.getY() ) ){
                    moveLeft = true;
                } else if( controls.goRight.contains( (int)event.getX(), (int)event.getY() ) ){
                    moveRight = true;
                }
                break;
            case MotionEvent.ACTION_UP:
                moveLeft = false;
                moveRight = false;
                break;
            case MotionEvent.ACTION_MOVE:
                if( !controls.goLeft.contains( (int)event.getX(), (int)event.getY() ) ){
                    moveLeft = false;
                } else if( controls.goLeft.contains( (int)event.getX(), (int)event.getY() ) ) {
                    moveLeft = true;
                }
                if( !controls.goRight.contains( (int)event.getX(), (int)event.getY() ) ){
                    moveRight = false;
                } else if( controls.goRight.contains( (int)event.getX(), (int)event.getY() ) ){
                    moveRight = true;
                }
                break;
        }
        return true;
    }

    public void reset(){
        playerXPos = 4 * Constants.SCREEN_WIDTH / 5;
        playerYPos = (int) ( Constants.SCREEN_HEIGHT * 0.615 );
        playerPoint = new Point(playerXPos, playerYPos);
        player.update( playerPoint );
        canibalManager.resetPosition();
        jump = false;
    }

    public void update(){
        if( moveLeft ){
            if( playerXPos <= 0 ){
                playerXPos = 0;
            } else {
                playerXPos -= 10;
            }
        } else if( moveRight ){
            if( playerXPos >= Constants.SCREEN_WIDTH ){
                playerXPos = Constants.SCREEN_WIDTH;
            } else {
                playerXPos += 10;
            }
        }

        if( jump == true ) {
            playerYPos -= 10;
            if( playerYPos <= Constants.SCREEN_HEIGHT * 0.3 ) {
                playerYPos = (int) ( Constants.SCREEN_HEIGHT * 0.3 );
                jump = false;
            }
        } else if( jump == false ){
            playerYPos += 10;
            if( playerYPos >= Constants.SCREEN_HEIGHT * 0.615 ){
                playerYPos = (int) ( Constants.SCREEN_HEIGHT * 0.615 );
            }
        }

        if( playerXPos == 0 ){
            player.score += 100;
            playerXPos = 4 * Constants.SCREEN_WIDTH / 5;
            playerYPos = (int) ( Constants.SCREEN_HEIGHT * 0.615 );
        }

        if( canibalManager.collide( player ) ){
            elapsedTime = System.currentTimeMillis();
            player.removeLife();
            reset();
        }

        playerPoint.set( playerXPos, playerYPos );
        player.update( playerPoint );
        canibalManager.update();
    }

    @Override
    public void draw( Canvas canvas ) {
        super.draw(canvas);
        Paint paint = new Paint();

        canvas.drawColor( Color.rgb( 0, 100, 255 ) );
        player.draw( canvas );
        canibalManager.draw( canvas );

        paint.setColor( Color.YELLOW );
        canvas.drawRect( (float) 0, (float) ( Constants.SCREEN_HEIGHT * 0.65 ), (float) ( Constants.SCREEN_WIDTH ), (float) ( Constants.SCREEN_HEIGHT ), paint );

        controls.draw( canvas );

        if( player.life.size() == 0 ){
            paint.setTextSize( (float) (Constants.SCREEN_HEIGHT * 0.14) );
            paint.setColor( Color.MAGENTA );
            player.gameOverText( canvas, paint, "Game Over" );
        }
    }
}
