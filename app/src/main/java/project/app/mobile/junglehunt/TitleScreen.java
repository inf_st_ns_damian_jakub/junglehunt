package project.app.mobile.test2;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.view.View;

public class TitleScreen extends View{
    Paint paint = null;
    public TitleScreen( Context context ){
        super(context);
        paint = new Paint();
    }

    @Override
    protected void onDraw( Canvas canvas ) {
        super.onDraw(canvas);
        int x = getWidth();
        int y = getHeight();

        paint.setStyle(Paint.Style.FILL);

        //background color
        paint.setColor(Color.rgb( 0, 153, 51 ));
        canvas.drawPaint(paint);

        paint.setColor(Color.rgb( 0, 102, 204 ));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            canvas.drawRoundRect( (float) ( getWidth() * 0.05  ),
                    (float) ( getHeight() * 0.05 ),
                    (float) ( getWidth() * 0.95 ),
                    (float) ( getHeight() * 0.95 ),
                    40,
                    40,
                    paint);
        } else {
            canvas.drawRect( (float) ( getWidth() * 0.05  ),
                    (float) ( getHeight() * 0.05 ),
                    (float) ( getWidth() * 0.95 ),
                    (float) ( getHeight() * 0.95 ),
                    paint);
        }

        //sign recangle
        paint.setColor(Color.rgb( 102, 51, 0));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            canvas.drawRoundRect( (float) ( getWidth() * 0.1 ),
                    (float) ( getHeight() * 0.2 ),
                    (float) ( getWidth() * 0.9 ),
                    (float) ( getHeight() * 0.5 ),
                    20,
                    20,
                    paint);
        } else {
            canvas.drawRect((float) ( getWidth() * 0.1 ),
                    (float) ( getHeight() * 0.2 ),
                    (float) ( getWidth() * 0.9 ),
                    (float) ( getHeight() * 0.5 ),
                    paint);
        }
        canvas.drawRect( (float) ( getWidth() * 0.45 ),
                (float) ( getHeight() * 0.5 ),
                (float) ( getWidth() * 0.55 ),
                (float) ( getHeight() * 0.7 ),
                paint);

        // Text size based on screen width
        paint.setColor(Color.WHITE);
        if ( getWidth() <= 800) {
            paint.setTextSize(60);
        } else if ( getWidth() > 800 && getWidth() <= 1280 ){
            paint.setTextSize(90);
        } else if ( getWidth() > 1280 && getWidth() <= 1920 ) {
            paint.setTextSize(120);
        } else if ( getWidth() > 1920 && getWidth() <= 2560 ) {
            paint.setTextSize(180);
        } else if ( getWidth() > 2560 && getWidth() <= 3840 ) {
            paint.setTextSize(240);
        }

        //Log.d("ScreenWidth: ", String.valueOf(getWidth())); //print screen width

        paint.setTextAlign(Paint.Align.CENTER);
        canvas.drawText("J  U  N  G  L  E", getWidth() / 2, (float) ( getHeight() * 0.3 ), paint);
        canvas.drawText("H  U  N  T", getWidth() / 2, (float) ( getHeight() * 0.45 ), paint);
        canvas.drawText("Tap to play", getWidth() / 2, (float) ( getHeight() * 0.85), paint);
    }
}
