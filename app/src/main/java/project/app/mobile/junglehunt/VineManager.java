package project.app.mobile.test2;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import java.util.ArrayList;

/**
 * Created by Dasmitian on 27.03.2018.
 */

public class VineManager {
    private ArrayList<Vine> vines;
    private int obstacleHeight;
    private int color;

    private long startTime;
    private long initTime;

    public int score;

    public VineManager(int playerGap, int obstacleGap, int obstacleHeight, int color ){
        this.obstacleHeight = obstacleHeight;
        this.color = color;

        startTime = initTime =  System.currentTimeMillis();

        vines = new ArrayList<>();

        populateObstacles();
    }

    public boolean playerCollide( Player player ){
        for( Vine ob : vines){
            if( ob.collide( player ) ){
                return true;
            }
        }
        return  false;
    }

    private void populateObstacles() {
        int currX = -5*Constants.SCREEN_WIDTH/4;
    }

    public void update(){
        int elapsedTime = (int)( System.currentTimeMillis() - startTime );
        startTime = System.currentTimeMillis();
        float speed = (float) ( Math.sqrt( 1+(startTime - initTime)/1000.0)) * Constants.SCREEN_HEIGHT/10000.0f;

    }

    /*
    public void update(){
        int elapsedTime = (int)( System.currentTimeMillis() - startTime );
        startTime = System.currentTimeMillis();
        float speed = (float) ( Math.sqrt( 1+(startTime - initTime)/1000.0)) * Constants.SCREEN_HEIGHT/10000.0f;
        for( Vine ob : vines){
            ob.incrementY( speed * elapsedTime );
        }
        if( vines.get( vines.size() - 1 ).getRectangle().top >= Constants.SCREEN_HEIGHT ){
            int xStart = (int)( Math.random() * ( Constants.SCREEN_WIDTH - playerGap ) );
            vines.add( 0, new Vine( obstacleHeight, color, xStart, vines.get( 0 ).getRectangle().top + obstacleHeight - obstacleGap, playerGap ) );
            vines.remove( vines.size() - 1 );
            score++;
        }
    }
     */

    public void draw( Canvas canvas ){
        for( Vine ob : vines){
            ob.draw( canvas );
        }
        Paint paint = new Paint();
        paint.setTextSize( (float) (Constants.SCREEN_HEIGHT * 0.14) );
        paint.setColor( Color.MAGENTA );
        canvas.drawText("" + score, 50, 50 + paint.descent() - paint.ascent(), paint);
    }
}
