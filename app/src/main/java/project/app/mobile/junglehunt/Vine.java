package project.app.mobile.test2;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Dasmitian on 27.03.2018.
 */

public class Vine implements GameObject {

    private Rect vineRect;
    private Paint paint;
    private Region region = new Region();
    private PathMeasure pathMeasure;
    private int color;
    private int currentPoint = 0;
    private float increaseX = 15;
    private float increaseCurveRadius = 3;
    public boolean collisionDetected = false;

    private ArrayList<Rect> rectArrayList = new ArrayList<>();

    //vine start positions
    private int vineWidth;
    double topX;
    double bottomX;
    private double topY;
    private double bottomY;
    private int curveRadius;
    private double amplitude = Constants.SCREEN_WIDTH * 0.15; //amplitude of bottom of vine

    public Vine( ){
    }

    public Vine( int vineWidth, double topX, double bottomX, double topY, double bottomY, int curveRadius, int increaseX ){
        this.vineWidth = vineWidth;
        this.topX = topX;
        this.bottomX = bottomX;
        this.topY = topY;
        this.bottomY = bottomY;
        this.curveRadius = curveRadius;
        this.increaseX = increaseX;
    }

    /*
    *   Calculate middle point ( midX and midY ) of vine to set curve with specified angle
    */
    private Path drawCurve( ) {
        final Path path = new Path();

        double midX = topX + ((bottomX - topX) / 1.7);
        double midY = topY + ((bottomY - topY) / 1.7);
        double xDiff = midX - topX;
        double yDiff = midY - topY;
        double angle = (Math.atan2(yDiff, xDiff) * (180 / Math.PI)) - 90;
        double angleRadians = Math.toRadians(angle);
        float pointX = (float) (midX + -curveRadius * Math.cos(angleRadians));
        float pointY = (float) (midY + -curveRadius * Math.sin(angleRadians));
        path.moveTo((float) topX, (float) topY);
        path.cubicTo((float) topX, (float) topY, pointX, pointY, (float) bottomX, (float) bottomY);

        region.setPath(path, new Region((int) topX, (int) topY, (int) bottomX, (int) bottomY));

        return path;
    }

    public int getVineRectX(){
        for( Rect rect : rectArrayList ) {
            return rect.left;
        }
        return vineRect.left;
    }


    /*
    * Collision detection. If player touches rect moving along vine, collision is detected.
    */
    public boolean collide(final Player player ){
        pathMeas();
        for( Rect rect : rectArrayList ){
            if( rect.setIntersect( player.getPlayer(), rect ) ){
                collisionDetected = true;
                return rect.setIntersect( player.getPlayer(), rect );
            } else {
                collisionDetected = false;
            }
        }
        if( vineRect.setIntersect( player.getPlayer(), vineRect) ){
            collisionDetected = true;
        } else{
            collisionDetected = false;
        }
        return vineRect.setIntersect( player.getPlayer(), vineRect );
    }

    /*
    * Path measure. Specify path along vine for rect to move on it.
    */
    public Rect pathMeas(){
        pathMeasure = new PathMeasure( drawCurve(), false );
        float segmentLen = pathMeasure.getLength() / 10;
        float pathPoint[] = { 0f, 0f };

        /*
        pathMeasure.getPosTan( segmentLen * currentPoint, pathPoint, null );
        rectArrayList.add( new Rect( (int) pathPoint[0], (int) pathPoint[1], (int) pathPoint[0] + 1, (int) pathPoint[1] + 1 ) );
        currentPoint = 5;
        */
        /*
        if( currentPoint <= 10 ){
            currentPoint++;
        } else {
            currentPoint = 0;
        }
        */

        if( currentPoint <= 10 ){
            pathMeasure.getPosTan( segmentLen * currentPoint, pathPoint, null );
            vineRect = new Rect( (int) pathPoint[0], (int) pathPoint[1], (int) pathPoint[0] + 1, (int) pathPoint[1] + 1 );
            if( collisionDetected == false ) {
                currentPoint++;
            }
        } else {
            currentPoint = 0;
        }
        return vineRect;
        //return rectArrayList;
    }

    /*
    * Function to make vine swing
    */
    private void swingVine(){
        bottomX += increaseX;
        if( bottomX > topX + amplitude ) {
            increaseX = -increaseX;
        } else if ( bottomX < topX - amplitude ) {
            increaseX = -increaseX;
        }

        if( bottomX > topX + amplitude || bottomX < topX - amplitude ){
            increaseCurveRadius = -increaseCurveRadius;
        }

        if( bottomX > topX ){
            curveRadius += increaseCurveRadius;
        } else if ( bottomX < topX ){
            curveRadius += increaseCurveRadius;
        }
    }

    public void resetPosition( int x, double xx, double xxx, double xxxx, double xxxxx, int xxxxxx, int xxxxxxx ){
        vineWidth = x;
        topX = xx;
        bottomX = xxx;
        topY = xxxx;
        bottomY = xxxxx;
        curveRadius = xxxxxx;
        increaseX = xxxxxxx;
    }

    @Override
    public void draw( Canvas canvas ){
        paint  = new Paint();
        paint.setAntiAlias( true );
        paint.setStyle( Paint.Style.STROKE );
        paint.setStrokeWidth( vineWidth );
        paint.setColor( Color.GREEN );
        canvas.drawPath( drawCurve(), paint );

        paint.setColor( Color.BLUE );
        for( Rect rect : rectArrayList ){
            if( currentPoint <= 11 )
                canvas.drawRect( rect, paint );
        }
        canvas.drawRect( pathMeas(), paint );

    }

    @Override
    public void update() {
        swingVine();
    }


    public void update( int increaseTopX, int increaseBottomX ) {
        topX += increaseTopX;
        bottomX += increaseBottomX;

    }
}
