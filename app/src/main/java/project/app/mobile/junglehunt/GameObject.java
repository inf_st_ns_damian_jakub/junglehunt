package project.app.mobile.test2;

import android.graphics.Canvas;
import android.graphics.Point;

/**
 * Created by Dasmitian on 27.03.2018.
 */

public interface GameObject {
    public void draw( Canvas canvas );
    public void update();
}
