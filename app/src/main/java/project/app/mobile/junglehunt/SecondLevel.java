package project.app.mobile.test2;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class SecondLevel extends SurfaceView implements SurfaceHolder.Callback{

    private MainThread thread;

    private Controls controls;

    private Player player;
    private CrocodileManager crocodileManager;
    private Point playerPoint;
    private Point crocodilePoint;
    private int playerXPos = 3 * Constants.SCREEN_WIDTH / 4;
    private int playerYPos = Constants.SCREEN_HEIGHT / 3;
    private int crocodileXPos = 0;
    private int crocodileYPos = Constants.SCREEN_HEIGHT / 2;
    private int crocodileMove = 10;
    private float oxygenLvl = (float) ( Constants.SCREEN_WIDTH * 0.95 - 2 );


    private boolean moveUp, moveDown, moveLeft, moveRight;
    private boolean attack = false;

    private long startTime, elapsedTime;

    public SecondLevel( Context context ){
        super( context );

        getHolder().addCallback( this );


        thread = new MainThread( getHolder(), this );

        player = new Player( new Rect( 0, 0, (int) (Constants.SCREEN_WIDTH * 0.04), (int) (Constants.SCREEN_HEIGHT * 0.07) ) );
        crocodilePoint = new Point( crocodileXPos, crocodileYPos );
        playerPoint = new Point(playerXPos, playerYPos);
        crocodileManager = new CrocodileManager();
        controls = new Controls();
        controls.drawActionButton = true;
        controls.drawMoveButton = true;
        controls.drawUpDown = true;
    }

    @Override
    public void surfaceChanged( SurfaceHolder holder, int format, int width, int height ){

    }

    @Override
    public void surfaceCreated( SurfaceHolder holder ){
        thread = new MainThread( getHolder(), this );
        thread.setRunning( true );
        thread.start();
    }


    @Override
    public void surfaceDestroyed( SurfaceHolder holder ){
        boolean retry = true;
        while( retry ){
            try{
                thread.setRunning( false );
                thread.join();
            } catch( Exception e ){ e.printStackTrace(); }
            retry = false;
        }
    }

    public void reset(){
        playerXPos = 3 * Constants.SCREEN_WIDTH / 4;
        playerYPos = Constants.SCREEN_HEIGHT / 3;
        playerPoint = new Point(playerXPos, playerYPos);
        player.update( playerPoint );
        attack = false;
        oxygenLvl = (float) ( Constants.SCREEN_WIDTH * 0.95 - 2 );
    }

    @Override
    public boolean onTouchEvent( MotionEvent event ) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if( controls.goUp.contains( (int)event.getX(), (int)event.getY() ) ){
                    moveUp = true;
                } else if( controls.goDown.contains( (int)event.getX(), (int)event.getY() ) ){
                    moveDown = true;
                } else if( controls.goLeft.contains( (int)event.getX(), (int)event.getY() ) ){
                    moveLeft = true;
                } else if( controls.goRight.contains( (int)event.getX(), (int)event.getY() ) ){
                    moveRight = true;
                }

                if( controls.action.contains( (int)event.getX(), (int)event.getY() ) ){
                    attack = true;
                    startTime = System.currentTimeMillis();
                    player.color = Color.YELLOW;
                }
                break;
            case MotionEvent.ACTION_UP:
                moveUp = moveDown = moveLeft = moveRight = false;
                break;
            case MotionEvent.ACTION_MOVE:
                if( !controls.goUp.contains( (int)event.getX(), (int)event.getY() ) ){
                    moveUp = false;
                } else if( controls.goUp.contains( (int)event.getX(), (int)event.getY() ) ) {
                    moveUp = true;
                }
                if( !controls.goDown.contains( (int)event.getX(), (int)event.getY() ) ){
                    moveDown = false;
                } else if( controls.goDown.contains( (int)event.getX(), (int)event.getY() ) ) {
                    moveDown = true;
                }
                if( !controls.goLeft.contains( (int)event.getX(), (int)event.getY() ) ){
                    moveLeft = false;
                } else if( controls.goLeft.contains( (int)event.getX(), (int)event.getY() ) ) {
                    moveLeft = true;
                }
                if( !controls.goRight.contains( (int)event.getX(), (int)event.getY() ) ){
                    moveRight = false;
                } else if( controls.goRight.contains( (int)event.getX(), (int)event.getY() ) ){
                    moveRight = true;
                }
                break;
        }
        return true;
    }

    public void update(){
        if( moveUp ) {
            if( playerYPos <= Constants.SCREEN_HEIGHT * 0.3 ){
                playerYPos = (int) ( Constants.SCREEN_HEIGHT * 0.3 );
            } else {
                playerYPos -= 10;
            }
        } else if( moveDown ){
            if( playerYPos == Constants.SCREEN_HEIGHT ){
                playerYPos = Constants.SCREEN_HEIGHT;
            } else {
                playerYPos += 10;
            }
        } else if( moveLeft ){
            if( playerXPos == 0 ){
                playerXPos = 0;
            } else {
                playerXPos -= 10;
            }
        } else if( moveRight ){
            if( playerXPos == Constants.SCREEN_WIDTH ){
                playerXPos = Constants.SCREEN_WIDTH;
            } else {
                playerXPos += 10;
            }
        }

        playerPoint.set( playerXPos, playerYPos );
        player.update( playerPoint );
        crocodileManager.update();

        if( attack == true && crocodileManager.collide( player ) ){
            crocodileManager.crocodiles.remove(crocodileManager.crocodiles.size() -1 );
            player.score += 100;
        } else if( attack == false && crocodileManager.collide( player ) ){
            player.removeLife();
            reset();
        }

        elapsedTime = System.currentTimeMillis();
        if( elapsedTime - startTime >= 1500 ){
            attack = false;
            startTime = 0;
            player.color = Color.RED;
        }

        if( playerYPos >= Constants.SCREEN_HEIGHT * 0.31 ){
            oxygenLvl -= 1;
        } else {
            oxygenLvl += 10;
            if( oxygenLvl >= (float) ( Constants.SCREEN_WIDTH * 0.95 - 2 ) ) {
                oxygenLvl = (float) (Constants.SCREEN_WIDTH * 0.95 - 2);
            }
        }

        if( oxygenLvl <= Constants.SCREEN_WIDTH * 0.75 - 2){
            player.removeLife();
            reset();
        }

    }

    @Override
    public void draw( Canvas canvas ) {
        super.draw( canvas );
        Paint paint = new Paint();

        canvas.drawColor( Color.rgb( 0, 100, 255 ) );
        paint.setColor( Color.argb( 196, 0, 0, 255 ) );
        canvas.drawRect( 0, (float) ( Constants.SCREEN_HEIGHT * 0.3 ), Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT, paint );

        player.draw( canvas );
        crocodileManager.draw( canvas );
        controls.draw( canvas );
        paint.setColor( Color. BLACK );
        canvas.drawRect( (float) ( Constants.SCREEN_WIDTH * 0.75 - 2 ), (float) ( Constants.SCREEN_HEIGHT * 0.05 - 2 ), (float) ( Constants.SCREEN_WIDTH * 0.95 + 2 ), (float) ( Constants.SCREEN_HEIGHT * 0.15 + 2 ), paint );
        paint.setColor( Color.WHITE );
        canvas.drawRect( (float) ( Constants.SCREEN_WIDTH * 0.75 ), (float) ( Constants.SCREEN_HEIGHT * 0.05 ), (float) ( Constants.SCREEN_WIDTH * 0.95 ), (float) ( Constants.SCREEN_HEIGHT * 0.15 ), paint );
        paint.setColor( Color.BLUE );
        canvas.drawRect( (float) ( Constants.SCREEN_WIDTH * 0.75 + 2 ), (float) ( Constants.SCREEN_HEIGHT * 0.05 + 2 ), oxygenLvl, (float) ( Constants.SCREEN_HEIGHT * 0.15 - 2 ), paint );

        if( player.life.size() == 0 ){
            paint.setTextSize( (float) (Constants.SCREEN_HEIGHT * 0.14) );
            paint.setColor( Color.MAGENTA );
            player.gameOverText( canvas, paint, "Game Over" );
        }

    }
}
