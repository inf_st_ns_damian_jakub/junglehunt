package project.app.mobile.test2;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.Log;

import java.util.ArrayList;

import static java.lang.String.valueOf;

public class RockManager {

    public ArrayList<Rock> rollingRock;
    public Point rollingRockPoint = new Point( 0, 0);

    private int yPos = (int) ( Constants.SCREEN_HEIGHT * 0.545 );
    private int incrementY = -10;
    private int incrementX = 20;

    public RockManager(){

        rollingRock = new ArrayList<>();
        rollingRockPoint.set( 0, yPos );
        rollingRock.add( new Rock( 0, 0, (int) (Constants.SCREEN_WIDTH * 0.065), (int) (Constants.SCREEN_HEIGHT * 0.105), rollingRockPoint) );
    }

    public boolean collide(final Player player ){
        for( Rock rock : rollingRock){
            if( rock.getRollingRock().intersects( player.getPlayer(), rock.getRollingRock() ) ){
                return true;
            }
        }
        return false;
    }

    public void resetPosition(){
        rollingRockPoint.set( 0, yPos );
        rollingRock.get( rollingRock.size() -1 ).resetPosition( rollingRockPoint );
    }

    public void update() {
        rollingRockPoint.set( 0, yPos );
        for( Rock rock : rollingRock ){
            rock.incrementX( incrementX );
        }

        rollingRock.get( rollingRock.size() -1 ).incrementY( incrementY );
        if( rollingRock.get( rollingRock.size() -1 ).getTop() <= Constants.SCREEN_HEIGHT * 0.4 || rollingRock.get( rollingRock.size() -1 ).getBottom() >= Constants.SCREEN_HEIGHT * 0.6325 ){
            incrementY = -incrementY;
        }

        if( rollingRock.get( rollingRock.size() - 1 ).getRollingRock().left >= Constants.SCREEN_WIDTH ){
            rollingRock.add( new Rock( 0, 0, (int) (Constants.SCREEN_WIDTH * 0.065), (int) (Constants.SCREEN_HEIGHT * 0.105), rollingRockPoint) );
            incrementX = (int) ((Math.random() * 20) + 10);
            incrementY = (int) ((Math.random() * 10) + 5 );
        }

        if( rollingRock.get( rollingRock.size() -1 ).getRollingRock().left >= Constants.SCREEN_WIDTH ){
            rollingRock.remove( rollingRock.size() -1 );
        }

    }

    public void draw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor( Color.GREEN );
        for ( Rock rock : rollingRock){
            rock.draw( canvas );
        }
    }
}
