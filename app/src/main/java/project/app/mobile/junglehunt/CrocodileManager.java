package project.app.mobile.test2;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;

import java.util.ArrayList;

public class CrocodileManager {

    public ArrayList<Crocodile> crocodiles;
    public Point crocodilePoint = new Point( 0, 0);

    private int yPos = (int) ( ( Math.random() * ( Constants.SCREEN_HEIGHT - Constants.SCREEN_HEIGHT * 0.1 ) ) + Constants.SCREEN_HEIGHT * 0.1 );

    public CrocodileManager(){

        crocodiles = new ArrayList<>();
        crocodilePoint.set( 0, -400 ); // rysuj pierwszego krokodyla poza ekranem, problemy z tablica i interakcja z pierwszym
        crocodiles.add( new Crocodile( 0, 0, (int) ( Constants.SCREEN_WIDTH * 0.12 ), (int) (Constants.SCREEN_HEIGHT * 0.14), crocodilePoint ) );
    }

    public boolean collide(final Player player ){
        for( Crocodile croc : crocodiles ){
            if( croc.getCrocodile().intersects( player.getPlayer(), croc.getCrocodile() ) ){
                return true;
            }
        }
        return false;
    }

    public void update() {
        yPos = (int) ( ( Math.random() * ( Constants.SCREEN_HEIGHT * 0.6 ) ) + Constants.SCREEN_HEIGHT * 0.4 );

        crocodilePoint.set( 0, yPos );
        for( Crocodile croc : crocodiles ){
            croc.incrementX( 10 );
        }
        if( crocodiles.get( crocodiles.size() - 1 ).getCrocodile().right >= Constants.SCREEN_WIDTH / 1.25 ){
            crocodiles.add( new Crocodile( 0, 0, (int) ( Constants.SCREEN_WIDTH * 0.12 ), (int) (Constants.SCREEN_HEIGHT * 0.14), crocodilePoint ) );
        }

        if( crocodiles.get( crocodiles.size() -1 ).getCrocodile().left >= Constants.SCREEN_WIDTH ){
            crocodiles.remove( crocodiles.size() -1 );
        }
    }

    public void draw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor( Color.GREEN );
        for ( Crocodile croc : crocodiles ){
            croc.draw( canvas );
        }
    }
}
