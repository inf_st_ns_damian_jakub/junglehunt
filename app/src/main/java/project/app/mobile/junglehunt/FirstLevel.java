package project.app.mobile.test2;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Handler;
import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Dasmitian on 27.03.2018.
 */

public class FirstLevel extends SurfaceView implements SurfaceHolder.Callback {
    private MainThread thread;

    private RectF button = new RectF( Constants.SCREEN_WIDTH / 20, 3 * Constants.SCREEN_HEIGHT / 4, Constants.SCREEN_WIDTH / 6, Constants.SCREEN_HEIGHT );
    private Paint paint2 = new Paint();
    private RectF button2 = new RectF( Constants.SCREEN_WIDTH / 20, Constants.SCREEN_HEIGHT / 4, Constants.SCREEN_WIDTH / 6, Constants.SCREEN_HEIGHT / 3 );

    private Player player;
    private int playerX = 100;
    private int playerY = 100;
    private int playerXPos = 3 * Constants.SCREEN_WIDTH / 4;
    private int playerYPos = Constants.SCREEN_HEIGHT / 3;
    private Point playerPoint;
    private VineManager vineManager;
    private ArrayList<Vine> vineList = new ArrayList<>();
    private int arrayIndex = 0;
    private int jumpX = 15;
    private int jumpY = 10;
    private int jumpCount = 0;

    private long startTime;
    private long endTime;
    private long deltaTime;

    private boolean movingPlayer = false;
    private boolean gameOver = false;
    private boolean jump = false;
    private boolean waiting = false;

    int vineWidth = Constants.SCREEN_WIDTH / 200;
    double topX = Constants.SCREEN_WIDTH / 4;
    double bottomX =  Constants.SCREEN_WIDTH / 4;
    double topY = Constants.SCREEN_HEIGHT / 10;
    double bottomY = Constants.SCREEN_HEIGHT * 0.5;
    int curveRadius = 0;

    public FirstLevel( Context context ){
        super( context );

        getHolder().addCallback( this );

        thread = new MainThread( getHolder(), this );

        player = new Player( new Rect( 0, 0, (int) (Constants.SCREEN_WIDTH * 0.04), (int) (Constants.SCREEN_HEIGHT * 0.07) ) );
        playerPoint = new Point(playerXPos, playerYPos);

        vineManager = new VineManager( 200, 350, 75, Color.BLACK );
        vineList.add(arrayIndex, new Vine( vineWidth, topX + 500, bottomX + 500, topY, bottomY, curveRadius, 15 ) );
        vineList.add(arrayIndex + 1, new Vine( vineWidth, topX - topX, bottomX - bottomX, topY, bottomY, curveRadius, 20 ) );

        setFocusable( true );

        paint2.setColor( Color.argb(32, 255, 0, 0 ) );
    }

    public void reset(){
        playerXPos = 3 * Constants.SCREEN_WIDTH / 4;
        playerYPos = Constants.SCREEN_HEIGHT / 3;
        playerPoint = new Point(playerXPos, playerYPos);
        player.update( playerPoint );
        jumpCount = 0;
        paint2.setColor( Color.argb(32, 255, 0, 0 ) );
        jump = false;
        movingPlayer = false;
        jumpY = -jumpY;
        waiting = false;
        vineList.get(0).resetPosition(vineWidth, topX + 500, bottomX + 500, topY, bottomY, curveRadius, 15);
        vineList.get(1).resetPosition(vineWidth, topX - topX, bottomX - bottomX, topY, bottomY, curveRadius, 20);
    }

    @Override
    public void surfaceChanged( SurfaceHolder holder, int format, int width, int height ){

    }

    @Override
    public void surfaceCreated( SurfaceHolder holder ){
        thread = new MainThread( getHolder(), this );
        thread.setRunning( true );
        thread.start();
    }

    @Override
    public void surfaceDestroyed( SurfaceHolder holder ){
        boolean retry = true;
        while( retry ){
            try{
                thread.setRunning( false );
                thread.join();
            } catch( Exception e ){ e.printStackTrace(); }
            retry = false;
        }
    }

    @Override
    public boolean onTouchEvent( MotionEvent event ) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if( !gameOver && button.contains( (int)event.getX(), (int)event.getY() ) && paint2.getColor() == Color.argb( 32, 255, 0, 0 ) ){
                    jump = true;
                    jumpCount++;
                    paint2.setColor( Color.argb( 32, 0, 0, 255 ) );
                }
                if( !gameOver && button2.contains( (int)event.getX(), (int)event.getY() ) ){
                    reset();
                }
                break;
        }
        return true;
    }

    public boolean collision(){

        startTime = System.currentTimeMillis();
        if ( vineList.get(0).collide(player) && jump == true && jumpCount > 1 ||
                vineList.get(1).collide(player) && jump == true && jumpCount > 1 ) {
            jumpY = -jumpY;
            playerPoint.set(playerXPos, playerYPos);
            waiting = true;
            return true;
        } else if ( vineList.get(0).collide(player)) {
            paint2.setColor(Color.argb(32, 255, 0, 0));
            jump = false;
            playerPoint.set(vineList.get(0).getVineRectX(), playerYPos);
            playerXPos = vineList.get(0).getVineRectX();
            return true;

        } else if( vineList.get(1).collide( player ) ){
            paint2.setColor(Color.argb(32, 255, 0, 0));
            jump = false;
            playerPoint.set(vineList.get(1).getVineRectX(), playerYPos);
            playerXPos = vineList.get(1).getVineRectX();
            return true;
        }
        return false;
    }

    private void moveVine( int x, int y ){
        vineList.get(0).update( x, y);
        vineList.get(1).update( x, y);
    }

    public void update() {
        if( !gameOver ) {

            if( jump == true ){
                playerXPos -= jumpX;
                playerYPos -= jumpY;
                if( playerYPos < Constants.SCREEN_HEIGHT / 6 ) {
                    jumpY = -jumpY;
                }
                playerPoint.set(playerXPos, playerYPos);
            }
            if( waiting == true ){
                endTime = System.currentTimeMillis();
                deltaTime = endTime - startTime;
                if( deltaTime > 2000 ){
                    waiting = false;
                }
            } else if ( waiting == false ){
                if( collision() ){
                    if( vineList.get(0).topX < 3 * Constants.SCREEN_WIDTH / 4 ){
                        moveVine( 10, 10);
                    }
                }
            }

            player.update(playerPoint);
            vineManager.update();
            vineList.get(0).update();
            vineList.get(1).update();


            if (playerY > Constants.SCREEN_HEIGHT) {
                gameOver = true;
            }
        }
    }

    @Override
    public void draw( Canvas canvas ){
        super.draw( canvas );

        canvas.drawColor( Color.WHITE );

        player.draw( canvas );

        vineList.get(0).draw( canvas );
        vineList.get(1).draw( canvas );

        if( player.life.size() == 0 ){
            Paint paint = new Paint();
            paint.setTextSize( 100 );
            paint.setColor( Color.MAGENTA );
            player.gameOverText( canvas, paint, "Game Over" );
        }

        if( vineList.get(0).collide( player ) ) {
            vineManager.score++;
        }
        canvas.drawRoundRect( button, 300, 300, paint2);

        Paint paint3 = new Paint();
        paint3.setColor( Color.GREEN );
        canvas.drawRoundRect( button2, 300, 300, paint3);
    }
}
