package project.app.mobile.test2;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;

public class Rock implements GameObject {

    private Rect rollingRock;


    public Rock( int left, int top, int right, int bottom, Point point ){
        rollingRock = new Rect( left, top, right, bottom );
        rollingRock.set( point.x - rollingRock.width()/2, point.y - rollingRock.height()/2, point.x + rollingRock.width()/2, point.y + rollingRock.height()/2 );
    }

    public Rect getRollingRock(){
        return rollingRock;
    }

    public void incrementX( float x ){
        rollingRock.left += x;
        rollingRock.right += x;
    }

    public void incrementY( float y ){
        rollingRock.top += y;
        rollingRock.bottom += y;
    }

    public int getX(){
        return rollingRock.right;
    }

    public int getTop(){
        return rollingRock.top;
    }

    public int getBottom() { return  rollingRock.bottom; }

    public void resetPosition( Point point ){
        rollingRock.set( point.x - rollingRock.width()/2, point.y - rollingRock.height()/2 - 10, point.x + rollingRock.width()/2, point.y + rollingRock.height()/2 -10 );
    }

    @Override
    public void draw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor( Color.GRAY );
        canvas.drawCircle( rollingRock.centerX(), rollingRock.centerY(), (float) (Constants.SCREEN_HEIGHT * 0.07), paint );
        paint.setColor( Color.argb(0, 0, 0, 0 ) );
        canvas.drawRect( rollingRock, paint );

    }

    @Override
    public void update() {
    }

}
